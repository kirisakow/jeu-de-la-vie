package com.ankama.jdlv;

import java.util.Timer;
import java.util.TimerTask;

/**
 *
 */
public class App {


  public static void main(String[] args) {
    System.out.println("======================== Jeu de la vie ========================");
    final Board board;
    final int boardSize;
    final int refreshRate;
    // try to find and load a previously saved version of board
    Board savedVersionOfBoard = Board.safeLoadFromFile(Board.FILE_NAME_FOR_SERIALIZATION);
    // if found and loaded correctly
    if (savedVersionOfBoard != null) {
      board = savedVersionOfBoard;
      boardSize = board.getSize();
      refreshRate = board.getRefreshRate();
    }
    // if impossible to recover saved version
    else {
      // check mandatory parameters
      if (null == System.getProperty(Board.PARAM_NAME_BOARD_SIZE)
          || null == System.getProperty(Board.PARAM_NAME_REFRESH_RATE)) {
        // if parameters ko
        System.out.println(
            "Pour lancer Jeu de la vie correctement avec les paramètres obligatoires, utilisez le script BASH situé dans /src/main/resources/launcher.sh");
        System.exit(1);
      }
      // if parameters ok
      boardSize = Integer.parseInt(System.getProperty(Board.PARAM_NAME_BOARD_SIZE));
      refreshRate = Integer.parseInt(System.getProperty(Board.PARAM_NAME_REFRESH_RATE));
      board = new Board(boardSize, refreshRate);
      board.generateCells();
    }
    //
    Timer timer = new Timer();
    timer.schedule(new TimerTask() {
      @Override
      public void run() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
        //
        System.out.println("");
        System.out.println("======================== Jeu de la vie ========================");
        System.out.println("");
        System.out.println("Paramètres :");
        System.out.println(" ⋅ Taille de la grille : " + boardSize + " x " + boardSize + " cases");
        System.out.println(" ⋅ Taux de rafraîchissement : " + refreshRate + " ms");
        System.out.println(" ⋅ Part des cellules vivantes : " + (int) (100 * Board.SHARE_OF_LIVING_CELLS) + "%");
        System.out.println(" ⋅ Nombre de cellules voisines nécessaire pour maintenir ou créer la vie : "
            + CellAbstract.MINIMUM_NEIGHBORING_CELLS_NEEDED_FOR_LIFE + " à "
            + CellAbstract.MAXIMUM_NEIGHBORING_CELLS_NEEDED_FOR_LIFE);

        System.out.println("");
        //
        for (int i = 0; i < boardSize; i++) {
          for (int j = 0; j < boardSize; j++) {
            System.out.print(board.getCells()[i][j]);
          }
          System.out.print("\n");
        }
        //
        System.out.println("");
        board.howManyLivingCellsAtEveryUpdate.add(board.countLivingCells());
        for (Integer howManyLivingCells : board.howManyLivingCellsAtEveryUpdate) {
          System.out
              .println("Cellules vivantes sur un total de " + (boardSize * boardSize) + " : " + howManyLivingCells);
        }
        //
        Board.safeSaveToFile(board, Board.FILE_NAME_FOR_SERIALIZATION);
        //
        board.updateCells();
      }
    }, 0, refreshRate);

  }
}
