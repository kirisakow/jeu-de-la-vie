package com.ankama.jdlv;

public class Cell extends CellAbstract {
  //
  private static final long serialVersionUID = 8460578788357740985L;
  
  private int row;
  private int column;
  private boolean isAlive;
  private String avatar;

  public Cell(int row, int column) {
    super();
    this.row = row;
    this.column = column;
    if (Math.random() > (1 - Board.SHARE_OF_LIVING_CELLS)) {
      this.beAlive();
    } else {
      this.beDead();
    }

  }

  public int getRow() {
    return row;
  }

  public int getColumn() {
    return column;
  }

  public boolean isAlive() {
    return isAlive;
  }

  public void setAlive(boolean isAlive) {
    this.isAlive = isAlive;
  }

  public String getAvatar() {
    return avatar;
  }

  public void setAvatar(String avatar) {
    this.avatar = avatar;
  }

  public void beAlive() {
    this.setAlive(true);
    this.setAvatar(CellAbstract.livingCellAvatar);
  }

  public void beDead() {
    this.setAlive(false);
    this.setAvatar(CellAbstract.deadCellAvatar);
  }

  @Override
  public String toString() {
    return CellAbstract.leftWall + this.getAvatar() + CellAbstract.rightWall;
  }

  public Cell detectLivingCellsAroundAndReturnUpdated(Cell[][] cells) {
    int howManyLivingCellsAround = 0;
    howManyLivingCellsAround += checkIfNeighboringCellIsAlive(cells, this.getRow() - 1, this.getColumn() + 1);
    howManyLivingCellsAround += checkIfNeighboringCellIsAlive(cells, this.getRow(),     this.getColumn() + 1);
    howManyLivingCellsAround += checkIfNeighboringCellIsAlive(cells, this.getRow() + 1, this.getColumn() + 1);
    howManyLivingCellsAround += checkIfNeighboringCellIsAlive(cells, this.getRow() + 1, this.getColumn());
    howManyLivingCellsAround += checkIfNeighboringCellIsAlive(cells, this.getRow() + 1, this.getColumn() - 1);
    howManyLivingCellsAround += checkIfNeighboringCellIsAlive(cells, this.getRow(),     this.getColumn() - 1);
    howManyLivingCellsAround += checkIfNeighboringCellIsAlive(cells, this.getRow() - 1, this.getColumn() - 1);
    howManyLivingCellsAround += checkIfNeighboringCellIsAlive(cells, this.getRow() - 1, this.getColumn());
    //
    if (howManyLivingCellsAround >= MINIMUM_NEIGHBORING_CELLS_NEEDED_FOR_LIFE
        && howManyLivingCellsAround <= MAXIMUM_NEIGHBORING_CELLS_NEEDED_FOR_LIFE) {
      this.beAlive();
    } else {
      this.beDead();
    }
    return this;
  }

  private int checkIfNeighboringCellIsAlive(Cell[][] cells, int row, int column) {
    if (row < 0 || column < 0 || row == cells.length || column == cells.length) {
      return 0;
    } else {
      return cells[row][column].isAlive() ? 1 : 0;
    }
  }



}
