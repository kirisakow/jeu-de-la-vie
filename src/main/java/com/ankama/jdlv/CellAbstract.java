package com.ankama.jdlv;

import java.io.Serializable;

public class CellAbstract implements Serializable {
  //
  private static final long serialVersionUID = 2235513986398923349L;

  // minimum neighboring cells needed to keep a cell alive or create life in it
  public static final int MINIMUM_NEIGHBORING_CELLS_NEEDED_FOR_LIFE = 1;
  // maximum neighboring cells needed to keep a cell alive or create life in it
  public static final int MAXIMUM_NEIGHBORING_CELLS_NEEDED_FOR_LIFE = 5;

  protected static final String leftWall = "";
  protected static final String rightWall = "";
  protected static final String livingCellAvatar = "😃";
  protected static final String deadCellAvatar = "☠️ ";

}
