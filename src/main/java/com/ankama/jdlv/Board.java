package com.ankama.jdlv;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Board implements Serializable {
  //
  private static final long serialVersionUID = 6546726563868119974L;
  public static final String FILE_NAME_FOR_SERIALIZATION = "jdlv";

  // names of mandatory params
  public static final String PARAM_NAME_REFRESH_RATE = "refreshRate";
  public static final String PARAM_NAME_BOARD_SIZE = "boardSize";
  // share of living cells among all cells on the board
  public static final double SHARE_OF_LIVING_CELLS = 0.15;

  private int size;
  private int refreshRate;
  private Cell cells[][];
  public List<Integer> howManyLivingCellsAtEveryUpdate = new ArrayList<Integer>();

  public Board(int boardSize, int refreshRate) {
    this.setSize(boardSize);
    this.setRefreshRate(refreshRate);
    this.setCells(new Cell[boardSize][boardSize]);
  }

  public int getSize() {
    return size;
  }

  public void setSize(int size) {
    this.size = size;
  }

  public int getRefreshRate() {
    return refreshRate;
  }

  public void setRefreshRate(int refreshrate) {
    this.refreshRate = refreshrate;
  }

  public Cell[][] getCells() {
    return cells;
  }

  public void setCells(Cell[][] cells) {
    this.cells = cells;
  }

  public void generateCells() {
    for (int i = 0; i < getSize(); i++) {
      for (int j = 0; j < getSize(); j++) {
        cells[i][j] = new Cell(i, j);
      }
    }
  }

  public int countLivingCells() {
    int count = 0;
    for (Cell[] row : getCells()) {
      for (Cell cell : row) {
        if (cell.isAlive()) {
          count++;
        }
      }
    }
    return count;
  }

  public void updateCells() {
    for (int i = 0; i < getSize(); i++) {
      for (int j = 0; j < getSize(); j++) {
        cells[i][j] = cells[i][j].detectLivingCellsAroundAndReturnUpdated(getCells());
      }
    }
  }

  public static void safeSaveToFile(Board board, String fileName) {
    ObjectOutputStream oos;
    try {
      oos = new ObjectOutputStream(new FileOutputStream(fileName));
      oos.writeObject(board);
      oos.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public static Board safeLoadFromFile(String fileName) {
    Board savedVersionOfBoard = null;
    ObjectInputStream ois;
    try {
      ois = new ObjectInputStream(new FileInputStream(fileName));
      savedVersionOfBoard = (Board) ois.readObject();
    } catch (IOException | ClassNotFoundException e) {
      e.printStackTrace();
    }
    return savedVersionOfBoard;
  }


}
