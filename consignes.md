## Consignes du test
### Développer un projet "Jeu de la vie"


* ✅ Grille de taille paramétrable, dans le code ou en paramètre
* ✅ Fréquence de rafraichissement paramétrable
* ✅ Placement initial aléatoire de cellules, avec 15% des cellules vivantes
* ✅ Transition d’état, une cellule nait/vit si 1 à 5 cellules vivent dans les cellules adjacentes, sinon elle meurt
* ✅ Sauver l’état dans un fichier, recharger depuis le fichier s’il existe, et non corrompu
* ✅ Sauvegarde régulière
* ✅ Sauvegarde sur arrêt du processus
* ❌ Pouvoir réinitialiser le jeu de la vie sur commande
* Différents modes d’accès :
  * ✅ Mode basique solo, package autonome avec affichage
  * ❌ Mode réseau, un service central qui gère le jeu, avec possibilité de brancher n clients d’affichage TCP
* ✅ Rendu, au choix : console ou graphique – ce n’est pas cette partie qui sera validante pour le test
